import { IFingerPrintAuthPlugin, AvailableOptions } from './definitions';
export declare class FingerPrintAuth implements IFingerPrintAuthPlugin {
    available(): Promise<AvailableOptions>;
    verify(): Promise<any>;
    verifyWithFallback(): Promise<any>;
}
