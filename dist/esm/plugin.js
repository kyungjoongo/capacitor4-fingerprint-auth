import { Plugins } from '@capacitor/core';
const { FingerPrintAuthPlugin } = Plugins;
export class FingerPrintAuth {
    available() {
        return FingerPrintAuthPlugin.available();
    }
    verify() {
        return FingerPrintAuthPlugin.verify();
    }
    verifyWithFallback() {
        return FingerPrintAuthPlugin.verifyWithFallback();
    }
}
//# sourceMappingURL=plugin.js.map