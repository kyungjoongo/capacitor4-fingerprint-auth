import { WebPlugin } from '@capacitor/core';
export class FingerPrintAuthPluginWeb extends WebPlugin {
    constructor() {
        super({
            name: 'FingerPrintAuth',
            platforms: ['web']
        });
    }
    available() {
        return new Promise(() => { });
    }
    verify() {
        return new Promise(() => { });
    }
    verifyWithFallback() {
        return new Promise(() => { });
    }
}
const FingerPrintAuthWebPlugin = new FingerPrintAuthPluginWeb();
export { FingerPrintAuthWebPlugin };
//# sourceMappingURL=web.js.map