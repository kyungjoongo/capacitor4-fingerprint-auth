import { WebPlugin } from '@capacitor/core';
import { IFingerPrintAuthPlugin, AvailableOptions } from './definitions';
export declare class FingerPrintAuthPluginWeb extends WebPlugin implements IFingerPrintAuthPlugin {
    constructor();
    available(): Promise<AvailableOptions>;
    verify(): Promise<any>;
    verifyWithFallback(): Promise<any>;
}
declare const FingerPrintAuthWebPlugin: FingerPrintAuthPluginWeb;
export { FingerPrintAuthWebPlugin };
