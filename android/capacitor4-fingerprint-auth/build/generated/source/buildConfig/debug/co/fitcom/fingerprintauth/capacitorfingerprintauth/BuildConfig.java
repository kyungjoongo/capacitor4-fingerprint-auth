/**
 * Automatically generated file. DO NOT MODIFY
 */
package co.fitcom.fingerprintauth.capacitorfingerprintauth;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "co.fitcom.fingerprintauth.capacitorfingerprintauth";
  public static final String BUILD_TYPE = "debug";
}
